﻿using Blocks.Contracts.OrdersManagement;
using System;

namespace OrderAPI.WebHost.Models
{
    public class CouriersResponse
    {
        public CouriersResponse(Courier courier)
        {
            Id = courier.Id;
            FirstName = courier.FirstName;
            LastName = courier.LastName;
            Email = courier.Email;
        }

        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }
    }
}
