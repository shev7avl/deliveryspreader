﻿using System;

namespace OrderAPI.WebHost.Models
{
    public class OrderRequest
    {
        public Guid ProductId { get; set; }
        public int Amount { get;set; }
    }
}
