﻿using Blocks.Contracts.OrdersManagement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrderAPI.Core.Abstractions;
using OrderAPI.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductsController : ControllerBase
    {

        private readonly ILogger<ProductsController> _logger;
        private readonly IRepository<Product> _productRepository;

        public ProductsController(ILogger<ProductsController> logger, IRepository<Product> productRepository)
        {
            _logger = logger;
            _productRepository = productRepository;
        }

        /// <summary>
        /// Получить все продукты
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<ProductsResponse>> GetProductAsync()
        {
            var products = await _productRepository.GetAllAsync();

            var productsModelList = products.Select(x => new ProductsResponse(x)).ToList();

            return productsModelList;
        }

        /// <summary>
        /// Создать продукт
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="price"></param>
        /// <param name="deliveryprice"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateProductAsync(string name, string description, int price, int deliveryprice)
        {
            Product product = new Product() { Id = Guid.NewGuid(), Name = name, Description = description, Price = price, DeliveryPrice = deliveryprice};

            await _productRepository.CreateAsync(product);

            return Ok("Добавлен продукт");
        }

        /// <summary>
        /// Обновить продукт
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="price"></param>
        /// <param name="deliveryprice"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> UpdateProductAsync(Guid id, string name, string description, int price, int deliveryprice)
        {
            var product = await _productRepository.GetByIdAsync(id);

            if (product == null) return BadRequest();

            if(name != null) product.Name = name;

            if(description != null) product.Description = description;

            if(price != 0) product.Price = price;

            if(deliveryprice != 0) product.DeliveryPrice = deliveryprice;

            await _productRepository.UpdateAsync(product);

            return Ok("Данные обновлены");
        }

        /// <summary>
        /// Удалить продукт
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteProductAsync(Guid id)
        {
            var product = await _productRepository.GetByIdAsync(id);

            if (product == null) return BadRequest(new {message = "Incorrect id"});

            await _productRepository.DeleteAsync(product);

            return Ok("Продукт удален");
        }
    }
}
