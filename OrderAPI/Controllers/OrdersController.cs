﻿using Blocks.Contracts;
using Blocks.Contracts.OrdersManagement;
using EventBus.Base.Standard;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrderAPI.Core.Abstractions;
using OrderAPI.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class OrdersController : ControllerBase
    {

        private readonly ILogger<OrdersController> _logger;
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<Courier> _courierRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IEventBus _eventBus;

        public OrdersController(
            ILogger<OrdersController> logger, 
            IRepository<Order> orderRepository, 
            IRepository<Courier> courierRepository,
            IRepository<Product> productRepository,
            IEventBus eventBus)
        {
            _logger = logger;
            _orderRepository = orderRepository;
            _courierRepository = courierRepository;
            _productRepository = productRepository;
            _eventBus = eventBus;
        }

        /// <summary>
        /// Получить все заказы вместе с продуктами, клиентом и назначенным курьером
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<OrdersResponce>>> GetOrdersAllAsync()
        {
            await _orderRepository.GetAllAsync("Customer");

            await _orderRepository.GetAllAsync("Courier");

            var orders = await _orderRepository.GetAllAsync("Products");

            var ordersModelList = orders.Select(x => new OrdersResponce(x)).ToList();

            return ordersModelList;
        } 

        /// <summary>
        /// Создать заказ
        /// </summary>
        /// <param name="orderRequests"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        [HttpPost(nameof(CreateOrderAsync))]
        public async Task<ActionResult<Order>> CreateOrderAsync(List<OrderRequest> orderRequests, Guid customerId)
        {
            //List<Product> productList = new List<Product>();
            //foreach (var item in orderRequests)
            //{
            //    var product = await _productRepository.GetByIdAsync(item.ProductId);
            //    product.Price = product.Price * item.Amount;
            //    productList.Add(product);
            //}
            //Order order = new Order() { Id = Guid.NewGuid(), CustomerId = customerId, Status = OrderStatus.Created, CreationDate = DateTime.Now, Products = productList };
            Order order = new Order() {Id = Guid.NewGuid(), CustomerId = customerId, Status = OrderStatus.Created };
            //await _orderRepository.CreateAsync(order);
            var message = new CreatedOrderEvent(order);

            _eventBus.Publish(message);
            return StatusCode(201,"Заказ отправлен в обработку.");
        }

        /// <summary>
        /// Назначить курьера с courierid на конкретный заказ c id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="courierid"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> TakeOrderAsync(Guid id, Guid courierid)
        {
            var order = await _orderRepository.GetByIdAsync(id, "Courier");

            if (order == null) return BadRequest(new { message = "Incorrect id" });

            order.Status = OrderStatus.CourierAssigned;

            order.CourierAssignedDate = DateTime.Now;

            Courier courier = await _courierRepository.GetByIdAsync(courierid);

            order.Courier = courier;

            await _orderRepository.UpdateAsync(order);

            return Ok("Курьер назначен");
        }


        /// <summary>
        /// Удалить заказ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteOrderAsync(Guid id)
        {
            var order = await _orderRepository.GetByIdAsync(id);

            if (order == null) return BadRequest(new { message = "Incorrect id" });

            await _orderRepository.DeleteAsync(order);

            return Ok("Заказ удален");
        }
    }
}
