﻿using Blocks.Contracts.BaseEntitys;
using System.Collections.Generic;

namespace Blocks.Contracts.OrdersManagement
{
    public class Courier : BaseEntity
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
