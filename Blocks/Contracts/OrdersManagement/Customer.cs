﻿using Blocks.Contracts.BaseEntitys;
using System.Collections.Generic;
namespace Blocks.Contracts.OrdersManagement
{
    public class Customer : BaseEntity
    {
        public string FirstName { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
