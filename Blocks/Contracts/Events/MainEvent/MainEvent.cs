﻿using EventBus.Base.Standard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blocks.Contracts.Events
{
    public class MainEvent : IntegrationEvent
    {
        public Guid EventID { get; set; }
        public string Content { get; set; }
    }
}
