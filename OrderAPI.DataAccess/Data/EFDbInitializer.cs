﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderAPI.DataAccess.Data
{
    public class EFDbInitializer : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EFDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Couriers);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Products);
            _dataContext.SaveChanges();

            _dataContext.AddRange(FakeDataFactory.Orders);
            _dataContext.SaveChanges();
        }

    }
}
