﻿using Blocks.Contracts.OrdersManagement;
using Microsoft.EntityFrameworkCore;
using OrderAPI.WebHost;
using OrderAPI.DataAccess.Data;

namespace OrderAPI.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Courier> Couriers { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
          
            //optionsBuilder.UseSqlite("Filename = DeliveryDb.sqlite");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Инициализация базы данных

            modelBuilder.Entity<Order>().
                HasMany(o => o.Products).
                WithMany(p => p.Orders);

            //modelBuilder.Entity<Order>().HasOne(o => o.Customer).WithMany()
            //modelBuilder.Entity<Order>().HasData(FakeDataFactory.Orders);
            //modelBuilder.Entity<Product>().HasData(FakeDataFactory.Products);
            //modelBuilder.Entity<Courier>().HasData(FakeDataFactory.Couriers);
            //modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            //modelBuilder.Entity<Order>().HasMany(o => o.Products).WithMany(p => p.Orders)
            //            .UsingEntity(j => j.HasData(FakeDataFactory.OrderProducts));
        }
    }
}
