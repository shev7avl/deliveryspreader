﻿import {combineReducers} from "@reduxjs/toolkit";
import orderReducer from "./orderListSlice";
import profileReducer from "./profileSlice";

export default combineReducers({
    order: orderReducer,
    profile: profileReducer
});