﻿import React, {useEffect, useState} from "react";
import styles from './OrderList.module.css'
import {Pagination, Table} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {fetchOrdersAsync} from "../../store/reducers/orderListSlice";
import Loader from "../UI/Loader/Loader";
import Button from "../UI/Button/Button";

export const OrderList = () => {
    const orders = useSelector(state => state.order.orders);
    const status = useSelector(state => state.order.status);
    const error = useSelector(state => state.order.error);
    const ordersPerPage = useSelector(state => state.order.ordersPerPage);
    const numOfPages = useSelector(state => state.order.numOfPages);
    const [activePage, setActivePage] = useState(1);
    const [visibleOrders, setVisibleOrders] = useState([]);
    const dispatch = useDispatch();

    const renderOrders = (page = 1) => {
        console.log('render orders')
        //todo - change to visible orders or get from server only items for pagination
        if (visibleOrders.length === 0) {
            setVisibleOrders(orders.slice(0, ordersPerPage));
        }
        return visibleOrders.map(o => {
            //const products = JSON.stringify(o.products, null, 2);
            const productNames = o.products.map(x => x.name).join(', ');
            const totalPrice = o.products.map(x => x.price).reduce((sum, cur) => sum + cur, 0);

            return (
                <tr key={o.id}>
                    <td>{o.id}</td>
                    <td>{productNames}</td>
                    <td>{totalPrice}</td>
                    <td>{o.courier
                        ? o.courier.name
                        : <Button
                            type="primary"
                            onClick={takeOrder}
                        >
                            Take order
                        </Button>}</td>
                </tr>
            )
        });
    }

    useEffect(() => {
        if (status === 'idle') {
            console.log("effect")
            dispatch(fetchOrdersAsync());            
        }
        
    }, [status, dispatch])

    const changePage = page => {
        const start = (page - 1) * ordersPerPage;
        const end = start + ordersPerPage;
        setVisibleOrders(orders.slice(start, end));
        dispatch(fetchOrdersAsync());
        setActivePage(page);
    }

    const goToFirstPage = () => {
        setVisibleOrders(orders.slice(0, ordersPerPage));
        dispatch(fetchOrdersAsync());
        setActivePage(1);
    }

    const goToLastPage = () => {
        const start = (numOfPages - 1) * ordersPerPage;
        const end = start + ordersPerPage;
        setVisibleOrders(orders.slice(start, end));
        dispatch(fetchOrdersAsync());
        setActivePage(numOfPages);
    }

    const pagination = () => {
        const items = [];
        for (let page = 1; page <= numOfPages; page++) {
            items.push(
                <Pagination.Item
                    key={page}
                    active={page === activePage}
                    onClick={() => changePage(page)}
                >
                    {page}
                </Pagination.Item>,
            );
        }

        return (
            <div>
                <Pagination>
                    <Pagination.First
                        disabled={activePage === 1}
                        onClick={goToFirstPage}
                    />
                    <Pagination.Prev
                        disabled={activePage === 1}
                        onClick={() => changePage(activePage - 1)}
                    />
                    {items}
                    <Pagination.Next
                        disabled={activePage === numOfPages}
                        onClick={() => changePage(activePage + 1)}
                    />
                    <Pagination.Last
                        disabled={activePage === numOfPages}
                        onClick={goToLastPage}
                    />
                </Pagination>
            </div>
        );
    }

    const takeOrder = () => {
        console.log('take order is working');
    }
    
    let content
    
    if (status === 'loading') {
        content = <Loader/>
    } else if (status === 'success') {
        content = <React.Fragment>
            <Table bordered hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Order</th>
                    <th>Price</th>
                    <th>Courier</th>
                </tr>
                </thead>
                <tbody>
                {renderOrders()}
                </tbody>
            </Table>
            {pagination()}
        </React.Fragment>
    } else if (status === 'failed') {
        content = <h1>{error}</h1>
    }

    return (
        <div className={styles.OrderList}>
            { content }
        </div>
    )
}