﻿export function createControl(config, validation) {
    return {
        ...config,
        validation,
        valid: !validation,
        touched: false,
        value: ''
    }
}

function validateEmail(email) {
    return String(email)
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
}

export function validate(value, validation = null) {
    if (!validation) {
        return true;
    }
    
    let isValid = true;
    
    if (validation.required) {
        isValid = value.trim() !== '';
    }    

    if (validation.email) {
        isValid = validateEmail(value) && isValid;
    }
    
    if (validation.minLength) {
        isValid = value.length >= validation.minLength && isValid;
    }
    
    return isValid;
}

export function validateForm(formControls) {
    let isFormValid = true;
    
    for(let control in formControls) {
        if (formControls.hasOwnProperty(control)) {
            isFormValid = formControls[control].valid && isFormValid;
        }
    }
    
    return isFormValid;    
}