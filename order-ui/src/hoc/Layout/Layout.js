﻿import React, {Component} from "react";
import classes from './Layout.module.css'
import Sidebar from "../../components/Navigation/Sidebar/Sidebar";
import MenuToggle from "../../components/Navigation/MenuToggle/MenuToggle";
import Carousel from 'react-bootstrap/Carousel'
import HomeCarousel from "../../components/UI/HomeCarousel/HomeCarousel";

export default class Layout extends Component {
    state = {
        menu: false
    }

    toggleMenuHandled = () => {
        this.setState({
            menu: !this.state.menu
        })
    }

    menuCloseHandler = () => {
        this.setState({
            menu: false
        })
    }
    
    render() {
        return (
            <React.Fragment>
                <HomeCarousel/>
                <div className={classes.Layout}>
                    <Sidebar
                        isOpen = {this.state.menu}
                        onClose={this.menuCloseHandler}
                    />
                    <MenuToggle
                        onToggle={this.toggleMenuHandled}
                        isOpen={this.state.menu}
                    />
                    <main>
                        {this.props.children}
                    </main>
                </div>
            </React.Fragment>            
        )
    }
}