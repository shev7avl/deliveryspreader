﻿import React, {Component} from "react";
import classes from './Auth.module.css'
import {createControl, validate, validateForm} from "../../form/formFramework";
import Input from "../UI/Input/Input";
import Button from "../UI/Button/Button";
import withNavigate from "../../hoc/withNavigate";

function createFormControls() {
    const minPassLength = 3;
    return {
        email: createControl(
            {
                label: 'Email',
                type: 'email',
                errorMessage: "Can't be empty"
            },
            {
                required: true,
                email: true
            }
        ),
        password: createControl(
            {
                label: 'Password',
                type: 'password',
                errorMessage: "Password should be at least " + minPassLength
            },
            {
                required: true,
                minLength: minPassLength
            }
        )
    }
}

class Auth extends Component {

    state = {
        isFormValid: false,
        formControls: createFormControls(),
        mainError: ''
    }

    submitHandler = event => {
        event.preventDefault();
    }

    renderInputs() {
        return Object.keys(this.state.formControls).map((controlName, index) => {
            const control = this.state.formControls[controlName];

            return (
                <Input
                    key={controlName + index}
                    label={control.label}
                    type={control.type}
                    errorMessage={control.errorMessage}
                    value={control.value}
                    valid={control.valid}
                    shouldValidate={!!control.validation}
                    touched={control.touched}
                    onChange={event => this.changeHandler(event.target.value, controlName)}
                    onBlur={this.onBlurHandler.bind(this, controlName)}
                />
            )
        })
    }

    changeHandler(value, controlName) {
        const formControls = {...this.state.formControls};
        const control = {...formControls[controlName]};
        
        control.value = value;
        control.valid = validate(value, control.validation);
        
        formControls[controlName] = control;
        
        this.setState({
            formControls,
            isFormValid: validateForm(formControls)
        })
    }

    onBlurHandler(controlName) {
        const formControls = {...this.state.formControls};
        const control = {...formControls[controlName]};
        
        control.touched = true;
        
        formControls[controlName] = control;
        
        this.setState({formControls, isFormValid: validateForm(formControls)});
    }

    loginHandler = () => {
        const authData = {
            email: this.state.formControls.email.value,
            password: this.state.formControls.password.value
        }
        
        if (authData.email !== 'admin@mail.com' && authData.password !== '123') {
            this.state.mainError = 'Wrong email or password';
            return;
        }
        
        this.props.onAuthorizationChange();
        const {navigate} = this.props;
        navigate('/');
    }

    render() {
        return (
            <div className={classes.Auth}>
                <div>
                    <h1>Login</h1>
                    <form onSubmit={this.submitHandler}>
                        {this.renderInputs()}
                    </form>
                    <Button
                        type="primary"
                        onClick={this.loginHandler}
                        disabled={!this.state.isFormValid}
                    >
                        Login
                    </Button>                    
                    <span style={{color: 'red'}}>{this.state.mainError}</span>                    
                </div>                
            </div>
        )
    }
}

export default withNavigate(Auth);