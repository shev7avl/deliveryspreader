﻿import React, {useEffect, useState} from "react";
import styles from './ProductDetails.module.css'
import {useLocation} from "react-router-dom";
import Table from 'react-bootstrap/Table';
import Cookies from "universal-cookie/es6";
import Button from "../UI/Button/Button";

export const ProductDetails = () => {
    console.log('function')
    const location = useLocation();
    const [product] = useState(location.state.product);
    //todo - amount if in cart
    const [amount, setAmount] = useState(0);
    
    useEffect(() => {
        console.log('effect');
        const cookies = new Cookies();        
        const productsCookie = cookies.get('products');
        if (productsCookie) {
            const productInCookie = productsCookie.filter(x => x.product.id === product.id);
            if (productInCookie.length > 0) {
                setAmount(productInCookie[0].amount);
            }
        }
    }, [])

    function addToCartHandler() {
        const cookies = new Cookies();
        const productsCookie = cookies.get('products');
        const productCartDto = {
            amount: 1,
            product: product
        };
        const products = [productCartDto];
        setAmount(1);
        
        if (!productsCookie) {
            cookies.set('products', products);            
        } else {
            productsCookie.push(productCartDto);
            cookies.set('products', productsCookie);
        }
    }
    
    function increaseAmount() {
        const cookies = new Cookies();
        const productsCookie = cookies.get('products');

        const thisProductInCookie = productsCookie.filter(x => x.product.id === product.id);
        thisProductInCookie[0].amount = thisProductInCookie[0].amount + 1;
        cookies.set('products', productsCookie);
        setAmount(thisProductInCookie[0].amount);
    }

    function decreaseAmount() {
        const cookies = new Cookies();
        let productsCookie = cookies.get('products');

        const thisProductInCookie = productsCookie.filter(x => x.product.id === product.id);        
        thisProductInCookie[0].amount = thisProductInCookie[0].amount - 1;
        if (thisProductInCookie[0].amount === 0) {
            productsCookie = productsCookie.filter(x => x.amount > 0);
        }
        cookies.set('products', productsCookie);
        setAmount(thisProductInCookie[0].amount);
    }

    return (
        <div className={styles.ProductDetails}>
            <div>
                <span>
                <img src={require("../Product/images/test.jpg")}/>
            </span>
                <div>
                    <div className={styles.Row}>
                        <b><p>{product.name}</p></b>
                    </div>
                    <div className={styles.Row}>
                        <p>Цена</p>
                        <p>{product.price}</p>
                    </div>
                    <div className={styles.Row}>
                        <p>Стоимость доставки</p>
                        <p>{product.deliveryPrice}</p>
                    </div>
                    <div className={styles.Row}>
                        <p>Описание</p>
                        <p>{product.description}</p>
                    </div>
                </div>
            </div>
            <br/>
            {amount === 0 
                ? <Button
                    type="primary"
                    onClick={addToCartHandler}
                >
                    Add to cart
                </Button>
                : <div>
                    <Button
                        type="error"
                        onClick={decreaseAmount}
                    >-</Button>
                    <br/>
                    <p style={{margin: '10px'}}>{amount}</p>
                    <br/>
                    <Button
                        type="success"
                        onClick={increaseAmount}
                    >+</Button>
                </div>
            }
            
        </div>
    )
}