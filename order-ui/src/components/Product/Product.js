import React from "react";
import classes from './Product.module.css';
import Button from "../UI/Button/Button";
import Cookies from "universal-cookie/es6";

const Product = props => {
    
    return (
        <div className={classes.Product}>
            <div>
                <div style={{textAlign: 'center', width: '100%'}}>
                    <img src={require("./images/test.jpg")}/>
                </div>
                
                <div className={classes.InlineFlex}>
                    <p className={classes.ProductName}>{props.product.name}</p>
                    <p>₽{props.product.price}</p>
                </div>                
            </div>
        </div>
    )
}

export default Product;