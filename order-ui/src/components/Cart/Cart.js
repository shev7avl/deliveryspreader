﻿import React, {useEffect, useState} from "react";
import classes from './Cart.module.css';
import Cookies from "universal-cookie/es6";
import {useNavigate} from "react-router-dom";
import Button from "../UI/Button/Button";
import Loader from "../UI/Loader/Loader";
import axios from "../../axios/axios-orderApi";


const Cart = () => {
    const navigate = useNavigate();
    const [cookie] = useState(new Cookies());
    const [totalSum, setTotalSum] = useState(0);
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const cookieProducts = cookie.get('products');
        if (cookieProducts && cookieProducts.length > 0) {
            const totalS = cookieProducts.map(x => {
                return x.amount * x.product.price;
            }).reduce((sum, curr) => sum + curr);

            setTotalSum(totalS);
            setProducts([...cookieProducts]);
        }
        setLoading(false);
    }, [])

    function renderCart() {
        if (products.length > 0) {
            const content = products.map((productDto, index) => {
                return (
                    <tr key={index}>
                        <td>{productDto.product.name}</td>
                        <td>{productDto.product.price}</td>
                        <td>{productDto.amount}</td>
                    </tr>
                )
            })
            return (
                <div>
                    <table>
                        <thead>
                        <tr>
                            <th>Название</th>
                            <th>Цена, шт</th>
                            <th>Количество</th>
                        </tr>
                        </thead>
                        <tbody>
                        {content}
                        </tbody>
                    </table>
                    <br/>
                    <h5>Total summ: {totalSum}</h5>
                    <Button
                        type='primary'
                        onClick={createOrder}
                    >Create order</Button>
                </div>
            )
        } else {            
            return (
                <h3>Cart is empty</h3>
            )
        }
    }

    async function createOrder() {
        //todo: add server logic        
        try {
            const request = products.map(x => {
                return {
                    productId: x.product.id,
                    amount: x.amount
                }
            });
            const response = await axios.post(
                'Orders',
                request,
                {
                    params: {
                        customerId: '123456d6-1111-4a11-9c7b-eb9f14e1a111' //todo - pass here param from profile
                    }
                });
            cookie.remove('products');
            navigate('/');
        } catch (e) {
            window.alert('ошибка при создании заказа: ', e);
        }

    }

    let content

    if (loading) {
        content = <Loader/>
    } else {
        content = renderCart()
    }

    return (
        <div className={classes.Cart}>
            {content}
        </div>
    )
}

export default Cart;